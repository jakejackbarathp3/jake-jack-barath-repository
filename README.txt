﻿This project is only has the following contributors/editors:
Barath P.
Jack L.
Jake H.
Mr. Eric Ferrante

The assignment is to create a tile-based adventure game all under Java.

The project is currently under development.




OVERVIEW
Our game is set in a generic fantasy environment where a village is under attack by slimes

STORY
You are a hero who is passing by and must protect a village that is under attack by slimes!
Talk to the NPCs to learn how to defeat the slimes, save the village, and win the game.

CONTROLS AND MECHANICS
Arrow keys to control movement of the character
Press space to interact with environment and NPCs

USER INTERFACE
Upper screen stays still and character can move around in it
Answer NPCs and learn information through dialog boxes
