package model_jjb;

import java.awt.image.BufferedImage;

public class Tile {
        
        private BufferedImage icon;
        private boolean walkable;
	private String fileName;
        private int message;
        
	public Tile(BufferedImage icon, String fileName, boolean walkable, int message) {
                this.icon = icon;
		this.walkable = walkable;
		this.fileName = fileName;
                this.message = message;
	}
        
        public BufferedImage getImageIcon() {
		return icon;
	}
	
	public boolean getPropertyWalkable() {
		return walkable;
	}
        
        public int getMessageIndex(){
                return message;
        }
	
	public String getFileName() {
		return fileName;
	}
	
        public void setPropertyWalkable(boolean newValue) {
		walkable = newValue;
	}
        
	@Override
	public String toString() {
		return "[" + getFileName() + " " + walkable + "]";
	}
}
