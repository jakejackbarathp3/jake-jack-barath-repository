package model_jjb;

import url.URL_Reader;
import model_jjb.characters.*;
import model_jjb.spells.*;
import java.util.Scanner;
import controller_jjb.Controller;
import java.io.File;
import java.io.FileNotFoundException;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;

public class Model {

        //Variables for Model Information
        private Controller c;
        private int numRow;
        private int numCol;
        private Tile[][] currentTile;
        private String currentLevel;
        private ArrayList<String> allMessages = new ArrayList<>();
        private int currentState;
        
        //Characters in the Model
        private MainCharacter link;
        private Enemy[] enemies;
        
        //Spells in the Model
	private FireSpell fireSpell;
        private boolean canUseFireball = false;
        
        //Constants
        public final static int TILESCREEN_STATE = 10000;
        public final static int OVERWORLD_STATE = 10001;
        public final static int TEXT_STATE = 10002;
        
        public Model(Controller controller, int row, int col){
                c = controller;
                this.numRow = row;
                this.numCol = col;
                currentTile = new Tile[row][col];
                currentState = OVERWORLD_STATE;
                currentLevel = "level_1.txt";
                link = new MainCharacter(0, 5, "MAIN");
                initializeMessages();
                updateCurrentLevel(URL_Reader.LEVELS + currentLevel);
        }
        
        public void updateCurrentLevel(String URL) {
		File file = new File(URL);
		try {
			Scanner scan = new Scanner(file);
                        String f, walk;
                        int interact = 0;
                        boolean walkable;
                        int row, col, counter = 0;
			while (scan.hasNext()) {
                                row = counter / this.numRow;
                                col = counter % this.numCol;
				f = scan.next().substring(1);
                                walk = scan.next();
                                interact = Integer.parseInt(scan.next());
                                if(walk.equals("true")) walkable = true;
                                else walkable = false;
                                currentTile[row][col] = new Tile(readImage(f), f, walkable, interact);
                                counter++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
        
        public void setCanUseFireball(boolean newState){
                canUseFireball = newState;
        }
        
        public boolean getCanUseFireball(){
                return canUseFireball;
        }
        
        public void initializeMessages(){
                File f = new File(URL_Reader.SPEECH);
                try {
                        Scanner s = new Scanner(f);
                        while(s.hasNextLine()){
                                allMessages.add(s.nextLine());
                        }
                } catch (FileNotFoundException ex) {
                        ex.printStackTrace();
                }
        }
        
        public String getMessage(int index){
                return allMessages.get(index);
        }
        
        private BufferedImage readImage(String filename){
                BufferedImage img = null;
                try {
                        img = ImageIO.read(new File(URL_Reader.TILES+ filename));
                } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                }
                return img;
        }
        
        public boolean isWalkable(int row, int col){
                if (inBounds(row, col))
			if (currentTile[row][col].getPropertyWalkable())
				return true;
		return false;
        }
        
        public boolean inBounds(int row, int col) {
		if (row < currentTile.length && row >= 0)
			if (col < currentTile[0].length && col >= 0)
				return true;
			else
				return false;
		return false;
	}
        
        public void switchLevels(){
                if(currentLevel.equals("level_1.txt")){
                        this.numRow = 20;
                        this.numCol = 20;
                        currentTile = new Tile[numRow][numCol];
                        currentState = OVERWORLD_STATE;
                        currentLevel = "level_2.txt";
                        link = new MainCharacter(18, 4, "MAIN");
                        link.setDirection(MainCharacter.FACING_UP);
                } else if(currentLevel.equals("level_2.txt")){
                        this.numRow = 10;
                        this.numCol = 10;
                        currentTile = new Tile[numRow][numCol];
                        currentState = OVERWORLD_STATE;
                        currentLevel = "level_1.txt";
                        link = new MainCharacter(0, 4, "MAIN");
                        link.setDirection(MainCharacter.FACING_DOWN);
                }
                updateCurrentLevel(URL_Reader.LEVELS + currentLevel);
        }
        
        public void switchLevel1(){
                this.numRow = 10;
                this.numCol = 10;
                currentTile = new Tile[numRow][numCol];
                currentState = OVERWORLD_STATE;
                currentLevel = "level_1.txt";
                link = new MainCharacter(0, 3, "MAIN");
        }
        
        public Tile[][] getCurrentTile(){
                return currentTile;
        }
        
        public String getCurrentLevel(){
                return currentLevel;
        }
        
        public int getCurrentState(){
                return currentState;
        }
        
        public void setCurrentState(int state){
                currentState = state;
        }
        
        public model_jjb.characters.Character getCharacter(String name){
                return link;
        }
        
        public model_jjb.spells.Spells getSpell(){
		return fireSpell;
	}
}
