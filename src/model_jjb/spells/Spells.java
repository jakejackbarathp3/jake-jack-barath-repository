package model_jjb.spells;

import url.URL_Reader;
import java.awt.image.BufferedImage;

public interface Spells {
        
	public final String IMAGES = URL_Reader.SPELLS;
        public final static String FACING_UP = "UP";
        public final static String FACING_DOWN = "DOWN";
        public final static String FACING_LEFT = "LEFT";
        public final static String FACING_RIGHT = "RIGHT";
        public final static String[] dirArr = {FACING_UP, FACING_DOWN, FACING_LEFT, FACING_RIGHT};
	
        public void moveLeft();
        public void moveRight();
        public void moveUp();
        public void moveDown();

        public int getRow();
        public int getCol();
        public boolean getLearned();
        public void setLearned(boolean learned);
        public String getFileName();
        public BufferedImage getImg();
        public String getDirection();
        public void setDirection(String facing);

        @Override
        public String toString();
        public BufferedImage readImage(String facing);
}
