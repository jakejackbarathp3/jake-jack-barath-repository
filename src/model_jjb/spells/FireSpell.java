package model_jjb.spells;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class FireSpell implements Spells{
	
	private int row;
	private int col;
	private String filename;
        private boolean learned;
        private String currentDirection;
	private BufferedImage[] imgArr = new BufferedImage[4];
        
	public FireSpell(int row, int col){
		this.row = row;
		this.col = col;
		filename = "FIREBALL";
                learned = false;
                currentDirection = FACING_UP;
                for(int i = 0; i < 4; i++)
                        imgArr[i] = readImage(dirArr[i]);
	}
        
        @Override
	public BufferedImage readImage(String facing) {
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File(IMAGES + filename + "_" + facing + ".gif"));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return img;
	}
        
        @Override
        public void moveUp() {
                row--;
                currentDirection = FACING_UP;
        }

        @Override
        public void moveDown() {
                row++;
                currentDirection = FACING_DOWN;
        }
        
	@Override
        public void moveLeft() {
                col--;
                currentDirection = FACING_LEFT;
        }

        @Override
        public void moveRight() {
                col++;
                currentDirection = FACING_RIGHT;
        }

	@Override
	public int getRow() {
		return row;
	}

	@Override
	public int getCol() {
		return col;
	}

        @Override
        public boolean getLearned() {
                return learned;
        }
	
        @Override
        public void setLearned(boolean learned){
                this.learned = learned;
        }
        
	@Override
	public String getFileName() {
		return filename;
	}

	@Override
	public BufferedImage getImg() {
		switch(currentDirection){
		case FACING_UP:
			return imgArr[0];
		case FACING_DOWN:
			return imgArr[1];
		case FACING_LEFT:
			return imgArr[2];
		case FACING_RIGHT:
			return imgArr[3];
		default:
			return null;
		}
	}
        
        public BufferedImage getImg(String facing){
                switch(facing){
		case FACING_UP:
			return imgArr[0];
		case FACING_DOWN:
			return imgArr[1];
		case FACING_LEFT:
			return imgArr[2];
		case FACING_RIGHT:
			return imgArr[3];
		default:
			return null;
		}
        }

        @Override
        public String getDirection() {
                return currentDirection;
        }

        @Override
        public void setDirection(String facing) {
                currentDirection = facing;
        }
}
