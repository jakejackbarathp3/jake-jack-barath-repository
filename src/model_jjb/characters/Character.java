package model_jjb.characters;

import url.URL_Reader;
import java.awt.image.BufferedImage;

public interface Character {
        public final String IMAGES = URL_Reader.SPRITES;
        public final static String FACING_UP = "UP";
        public final static String FACING_DOWN = "DOWN";
        public final static String FACING_LEFT = "LEFT";
        public final static String FACING_RIGHT = "RIGHT";
        public final static String[] dirArr = {FACING_UP, FACING_DOWN, FACING_LEFT, FACING_RIGHT};
        
        public BufferedImage readImage(String facing);
        public void moveLeft();
        public void moveRight();
        public void moveUp();
        public void moveDown();
        public void move(int row, int col);
        
        public int getRow();
        public int getCol();
        public String getFileName();
        public BufferedImage getImg();
        public String getDirection();
        public void setDirection(String facing);
        
        @Override
        public String toString();
}
