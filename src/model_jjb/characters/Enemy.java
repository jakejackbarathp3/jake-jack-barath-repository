package model_jjb.characters;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Enemy implements Character{

	private int row, col;
	private String enemyFilename;
	private String currentDirection = FACING_DOWN;
	private boolean alive;
	private BufferedImage[] imgArr = new BufferedImage[4];

	public Enemy(int row, int col){
		this.row = row;
		this.col = col;
		enemyFilename = "ENEMY";
		alive = true;
                for(int i = 0; i < 4; i++)
                        imgArr[i] = readImage(dirArr[i]);
	}

	@Override
	public BufferedImage readImage(String facing) {
		// TODO Auto-generated method stub
		File f = new File(IMAGES + enemyFilename + "_" + facing + ".gif");
		BufferedImage img = null;
		try {
			img = ImageIO.read(f);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return img;
	}

	public boolean isAlive(){
		return alive;
	}

	public void setAlive(boolean isalive){
		alive = isalive;
	}

	public void moveRandom(){
		java.util.Random random = new java.util.Random();
		int x = random.nextInt(4);
		switch (x) {
		case 1:
			moveDown();
			setDirection(FACING_DOWN);
			break;
		case 2:
			moveLeft();
			setDirection(FACING_LEFT);
			break;
		case 3:
			moveRight();
			setDirection(FACING_RIGHT);
			break;
		case 0:
			moveUp();
			setDirection(FACING_UP);
			break;
		default:
			break;
		}
	}

	@Override
	public void moveLeft() {
		col--;
	}

	@Override
	public void moveRight() {
		col++;
	}

	@Override
	public void moveUp() {
		row--;
	}

	@Override
	public void moveDown() {
		row++;
	}

	@Override
	public void move(int row, int col) {
		this.row = row;
		this.col = col;
	}

	@Override
	public int getRow() {
		return row;
	}

	@Override
	public int getCol() {
		return col;
	}

	@Override
	public String getFileName() {
		return enemyFilename;
	}

	@Override
	public BufferedImage getImg() {
		switch(currentDirection){
		case FACING_UP:
			return imgArr[0];
		case FACING_DOWN:
			return imgArr[1];
		case FACING_LEFT:
			return imgArr[2];
		case FACING_RIGHT:
			return imgArr[3];
		default:
			return null;
		}
	}

	public BufferedImage getImg(String direction) {
		switch(direction){
		case FACING_UP:
			return imgArr[0];
		case FACING_DOWN:
			return imgArr[1];
		case FACING_LEFT:
			return imgArr[2];
		case FACING_RIGHT:
			return imgArr[3];
		default:
			return null;
		}
	}

	@Override
	public String getDirection() {
		return currentDirection;
	}

	@Override
	public void setDirection(String facing) {
		currentDirection = facing;
	}

}
