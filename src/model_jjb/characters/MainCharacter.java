package model_jjb.characters;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class MainCharacter implements Character{
        
        private int row, col;
        private String filename;
        private BufferedImage[] imgArr = new BufferedImage[4];
        private String currentDirection = FACING_DOWN;

        public MainCharacter(int row, int col, String filename) {
                this.row = row;
                this.col = col;
                this.filename = filename;
                for(int i = 0; i < 4; i++)
                        imgArr[i] = readImage(dirArr[i]);
        }

        @Override
        public BufferedImage readImage(String facing) {
                File f = new File(IMAGES + filename + "_" + facing + ".gif");
                BufferedImage img = null;
                try {
                        img = ImageIO.read(f);
                } catch (IOException ex) {
                        ex.printStackTrace();
                }
                return img;
        }

        @Override
        public void moveLeft() {
                col--;
        }

        @Override
        public void moveRight() {
                col++;
        }

        @Override
        public void moveUp() {
                row--;
        }

        @Override
        public void moveDown() {
                row++;
        }

        @Override
        public void move(int row, int col) {
                this.row = row;
                this.col = col;
        }

        @Override
        public int getRow() {
                return row;
        }

        @Override
        public int getCol() {
                return col;
        }

        @Override
        public String getFileName() {
                return filename;
        }
        
        @Override
        public String getDirection(){
                return currentDirection;
        }
        
        @Override
        public void setDirection(String facing){
                currentDirection = facing;
        }

        @Override
        public BufferedImage getImg() {
                switch(currentDirection){
                        case FACING_UP:
                                return imgArr[0];
                        case FACING_DOWN:
                                return imgArr[1];
                        case FACING_LEFT:
                                return imgArr[2];
                        case FACING_RIGHT:
                                return imgArr[3];
                        default:
                                return null;
                }
        }
}
