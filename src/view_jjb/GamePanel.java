package view_jjb;

import url.URL_Reader;
import controller_jjb.Controller;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class GamePanel extends JPanel{

	public Controller c;

	public GamePanel(Controller controller){
		c = controller;
	}

	@Override
	public void paintComponent(Graphics g){
		drawWorld(g);
		drawChar(g);
		drawSpell(g);
		drawTime(g);
		if(c.getEnemyAlive()) drawEnemy(g);
		if(c.getEnemy2Alive()) drawEnemy2(g);
		if(c.getEnemy3Alive()) drawEnemy3(g);
		if(c.getEnemy4Alive()) drawEnemy4(g);
		if(c.getEnemy5Alive()) drawEnemy5(g);
		if(c.getCurrentState() == 10002) drawTextBox(g, c.getCurrentMessage());
	}

	private void drawTime(Graphics g) {
		int size = View.FRAME_SIZE/c.getCurrentTile().length;
		int rowMain = 15;
		int colMain = 2;
                
		Graphics2D graphics2d = (Graphics2D) g;
		graphics2d.setFont(new java.awt.Font("Arial", 100, 100));
		graphics2d.drawString("" + c.getTime() / 100, colMain*size, rowMain*size);
	}

	public void drawWorld(Graphics g){
		int size = View.FRAME_SIZE/c.getCurrentTile().length;
		for(int row = 0; row < c.getCurrentTile().length; row++){
			for(int col = 0; col < c.getCurrentTile()[0].length; col++){
				BufferedImage b = c.getCurrentTile()[row][col].getImageIcon();
				g.drawImage(b, col*size, row*size, size, size, null);
			}
		}
	}

	public void drawChar(Graphics g){
		int size = View.FRAME_SIZE/c.getCurrentTile().length;
		int rowMain = c.getCharacter("MAIN").getRow();
		int colMain = c.getCharacter("MAIN").getCol();

		BufferedImage b = c.getCharacter("MAIN").getImg();
		g.drawImage(b, colMain*size, rowMain*size, size, size, null);
	}

	public void drawEnemy(Graphics g){
		int size = View.FRAME_SIZE/c.getCurrentTile().length;

		int rowEnemy = c.getEnemy("null").getRow();
		int colEnemy = c.getEnemy("null").getCol();

		BufferedImage ene = c.getEnemy("null").getImg();
		g.drawImage(ene, colEnemy*size, rowEnemy*size, size, size, null);
	}

	public void drawEnemy2(Graphics g){
		int size = View.FRAME_SIZE/c.getCurrentTile().length;

		int rowEnemy = c.getEnemy2("null").getRow();
		int colEnemy = c.getEnemy2("null").getCol();

		BufferedImage ene = c.getEnemy2("null").getImg();
		g.drawImage(ene, colEnemy*size, rowEnemy*size, size, size, null);
	}
        
	public void drawEnemy3(Graphics g){
		int size = View.FRAME_SIZE/c.getCurrentTile().length;

		int rowEnemy = c.getEnemy3("null").getRow();
		int colEnemy = c.getEnemy3("null").getCol();

		BufferedImage ene = c.getEnemy3("null").getImg();
		g.drawImage(ene, colEnemy*size, rowEnemy*size, size, size, null);
	}
        
	public void drawEnemy4(Graphics g){
		int size = View.FRAME_SIZE/c.getCurrentTile().length;

		int rowEnemy = c.getEnemy4("null").getRow();
		int colEnemy = c.getEnemy4("null").getCol();

		BufferedImage ene = c.getEnemy4("null").getImg();
		g.drawImage(ene, colEnemy*size, rowEnemy*size, size, size, null);
	}
        
	public void drawEnemy5(Graphics g){
		int size = View.FRAME_SIZE/c.getCurrentTile().length;

		int rowEnemy = c.getEnemy5("null").getRow();
		int colEnemy = c.getEnemy5("null").getCol();

		BufferedImage ene = c.getEnemy5("null").getImg();
		g.drawImage(ene, colEnemy*size, rowEnemy*size, size, size, null);
	}

	public void drawSpell(Graphics g) {
		if(c.getSpell() != null){
			int size = View.FRAME_SIZE/c.getCurrentTile().length;
			int row = c.getSpell().getRow();
			int col = c.getSpell().getCol();
			BufferedImage b = c.getSpell().getImg();
			g.drawImage(b, col*size, row*size, size, size, null);
		}else{
                        //nothing
		}
	}

	public void drawTextBox(Graphics g, String message){
                Graphics2D graphics2d = (Graphics2D) g;
		
		graphics2d.setFont(new java.awt.Font("Arial", 20, 20));
		File f = new File(URL_Reader.OVERLAY + "textBox.gif");
		BufferedImage o = null;
		try {
			o = ImageIO.read(f);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		g.drawImage(o, 0, -25, View.FRAME_SIZE, View.FRAME_SIZE, null);
		g.setColor(Color.WHITE);
		g.drawString(message, 50, View.FRAME_SIZE-100);
	}
}
