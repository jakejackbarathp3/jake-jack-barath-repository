package view_jjb;

import controller_jjb.Controller;
import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.*;

public class View {

	public final static int FRAME_SIZE = 750;

	public Controller c;

	private JFrame window;
	private GamePanel gp;

	public View(Controller controller){

		c = controller;
		gp = new GamePanel(controller);

		//GUI Code
		window = new JFrame("Spells");
		window.setBounds(50, 50, FRAME_SIZE, FRAME_SIZE);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setLayout(new BorderLayout());
		window.setResizable(false);

		window.getContentPane().add(gp);
		window.setVisible(true);
	}

	public void addControls(KeyListener k){
		window.addKeyListener(k);
	}

	public void repaint(){
		gp.repaint();
	}
	
	public GamePanel getGamePanel(){
		return gp;
	}
}
