package url;


//helps with reading all the URLs needed to run the game
public class URL_Reader {
        public static String LEVELS = "resources/levels/";
        public final static String TILES = "resources/tiles/";
        public final static String SPEECH = "resources/text_speech.txt";
        public final static String SPRITES = "resources/sprites/";
	public final static String SPELLS = "resources/spell/";
	public final static String OVERLAY = "resources/overlays/";
}
