package controller_jjb;

import java.awt.event.ActionEvent;
import model_jjb.*;
import model_jjb.characters.*;
import view_jjb.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JOptionPane;

import model_jjb.spells.FireSpell;

public class Controller {

	public Model m;
	public View v;
	private String message;
	FireSpell fireSpell;
	boolean stopFireBall = false;
	int fireDistance = 0;
	Enemy enemy;
	Enemy enemy2;
	Enemy enemy3;
	Enemy enemy4;
	Enemy enemy5;
	int time = 6000;
	int enemyCounter = 5;

	public Controller(){
		m = new Model(this, 10, 10);
		v = new View(this);

		int enemyRow = 15;
		int enemyCol = 15;
		enemy = new Enemy(enemyRow, enemyCol);

		javax.swing.Timer t = new javax.swing.Timer(1000, null);
		t.addActionListener(new java.awt.event.ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(!(enemy.getCol() > enemyCol + 1 || enemy.getCol() < enemyCol - 1)){
					if(!(enemy.getRow() > enemyRow + 1 || enemy.getRow() < enemyRow - 1))
						enemy.moveRandom();
					else 
						enemy.move(enemyRow, enemyCol);
				}else {
					enemy.move(enemyRow, enemyCol);
				}
				if(!m.isWalkable(enemy.getRow(), enemy.getCol()))
					enemy.move(enemyRow, enemyCol);
				time -= 100;
				
				
				if(time == 0){
					JOptionPane.showMessageDialog(v.getGamePanel(), "Game Over");
					v.getGamePanel().setVisible(false);
					t.stop();
				}
				
				
				v.repaint();
				
			}
			
		});
				

		int enemy2Row = 10;
		int enemy2Col = 15;
		enemy2 = new Enemy(enemy2Row, enemy2Col);

		javax.swing.Timer t2 = new javax.swing.Timer(1000, new java.awt.event.ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(!(enemy2.getCol() > enemy2Col + 1 || enemy2.getCol() < enemy2Col - 1)){
					if(!(enemy2.getRow() > enemy2Row + 1 || enemy2.getRow() < enemy2Row - 1))
						enemy2.moveRandom();
					else 
						enemy2.move(enemy2Row, enemy2Col);
				}else {
					enemy2.move(enemy2Row, enemy2Col);
				}
				if(!m.isWalkable(enemy2.getRow(), enemy2.getCol()))
					enemy2.move(enemy2Row, enemy2Col);
				v.repaint();
			}
		});

		int enemy3Row = 15;
		int enemy3Col = 10;
		enemy3 = new Enemy(enemy3Row, enemy3Col);

		javax.swing.Timer t3 = new javax.swing.Timer(1000, new java.awt.event.ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(!(enemy3.getCol() > enemy3Col + 1 || enemy3.getCol() < enemy3Col - 1)){
					if(!(enemy3.getRow() > enemy3Row + 1 || enemy3.getRow() < enemy3Row - 1))
						enemy3.moveRandom();
					else 
						enemy3.move(enemy3Row, enemy3Col);
				}else {
					enemy3.move(enemy3Row, enemy3Col);
				}
				if(!m.isWalkable(enemy3.getRow(), enemy3.getCol()))
					enemy3.move(enemy3Row, enemy3Col);
				v.repaint();
			}
		});

		int enemy4Row = 12;
		int enemy4Col = 15;
		enemy4 = new Enemy(enemy4Row, enemy4Col);

		javax.swing.Timer t4 = new javax.swing.Timer(1000, new java.awt.event.ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(!(enemy4.getCol() > enemy4Col + 1 || enemy4.getCol() < enemy4Col - 1)){
					if(!(enemy4.getRow() > enemy4Row + 1 || enemy4.getRow() < enemy4Row - 1))
						enemy4.moveRandom();
					else 
						enemy4.move(enemy4Row, enemy4Col);
				}else {
					enemy4.move(enemy4Row, enemy4Col);
				}
				if(!m.isWalkable(enemy4.getRow(), enemy4.getCol()))
					enemy4.move(enemy4Row, enemy4Col);
				v.repaint();
			}
		});

		int enemy5Row = 15;
		int enemy5Col = 12;
		enemy5 = new Enemy(enemy5Row, enemy5Col);

		javax.swing.Timer t5 = new javax.swing.Timer(1000, null);
		t5.addActionListener(new java.awt.event.ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(!(enemy5.getCol() > enemy5Col + 1 || enemy5.getCol() < enemy5Col - 1)){
					if(!(enemy5.getRow() > enemy5Row + 1 || enemy5.getRow() < enemy5Row - 1))
						enemy5.moveRandom();
					else 
						enemy5.move(enemy5Row, enemy5Col);
				}else {
					enemy5.move(enemy5Row, enemy5Col);
				}
				if(!m.isWalkable(enemy5.getRow(), enemy5.getCol()))
					enemy5.move(enemy5Row, enemy5Col);
				
				int enemyCounter = 0;
				if(enemy.isAlive()){
					enemyCounter++;
				}else if (enemy2.isAlive()) {
					enemyCounter++;
				}else if (enemy3.isAlive()) {
					enemyCounter++;
				}else if (enemy4.isAlive()) {
					enemyCounter++;
				}else if (enemy5.isAlive()) {
					enemyCounter++;
				}
				Controller.this.enemyCounter = enemyCounter;
				if(enemyCounter == 0){
                                        t5.stop();
					JOptionPane.showMessageDialog(v.getGamePanel(), "You Won");
					v.getGamePanel().setVisible(false);
					//t5.stop();
				}
				v.repaint();
			}
			
			
		});
		t.start();
		
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		v.addControls(new KeyAdapter(){
			@Override
			public void keyPressed(KeyEvent e){
				MainCharacter main = (MainCharacter)m.getCharacter("MAIN");
				String direction = main.getDirection();

				int drow = main.getRow(), dcol = main.getCol();

				if(direction.equals(MainCharacter.FACING_DOWN)){
					drow++;
				} else if(direction.equals(MainCharacter.FACING_UP)){
					drow--;
				} else if(direction.equals(MainCharacter.FACING_LEFT)){
					dcol--;
				} else if(direction.equals(MainCharacter.FACING_RIGHT)){
					dcol++;
				}

				Tile tile = null;
				if(m.inBounds(drow, dcol)) tile = m.getCurrentTile()[drow][dcol];

				if(e.getKeyCode() == 37 && m.getCurrentState() == m.OVERWORLD_STATE){
					main.moveLeft();
					main.setDirection(MainCharacter.FACING_LEFT);
					if(!m.inBounds(main.getRow(), main.getCol()))
						m.switchLevels();
					else if(!m.isWalkable(main.getRow(), main.getCol()))
						main.moveRight();
				} else if(e.getKeyCode() == 38 && m.getCurrentState() == m.OVERWORLD_STATE){
					main.moveUp();
					main.setDirection(MainCharacter.FACING_UP);
					if(!m.inBounds(main.getRow(), main.getCol()))
						m.switchLevels();
					else if(!m.isWalkable(main.getRow(), main.getCol()))
						main.moveDown();
				} else if(e.getKeyCode() == 39 && m.getCurrentState() == m.OVERWORLD_STATE){
					main.moveRight();
					main.setDirection(MainCharacter.FACING_RIGHT);
					if(!m.inBounds(main.getRow(), main.getCol()))
						m.switchLevels();
					else if(!m.isWalkable(main.getRow(), main.getCol()))
						main.moveLeft();
				} else if(e.getKeyCode() == 40 && m.getCurrentState() == m.OVERWORLD_STATE){
					main.moveDown();
					main.setDirection(MainCharacter.FACING_DOWN);
					if(!m.inBounds(main.getRow(), main.getCol()))
						m.switchLevels();
					else if(!m.isWalkable(main.getRow(), main.getCol()))
						main.moveUp();

				} else if(e.getKeyCode() == 32){ //space
					if(m.getCurrentState() == m.TEXT_STATE){
						m.setCurrentState(m.OVERWORLD_STATE);
					} else {
						m.setCurrentState(m.TEXT_STATE);
						setCurrentMessage(m.getMessage(tile.getMessageIndex()));
                                                if(tile.getMessageIndex() == 2){
                                                        m.setCanUseFireball(true);
                                        
                                                }
					}
                                        
                                        
				}else if (e.getKeyChar() == 'b') {
                                        if(m.getCanUseFireball()){
                                                fireSpell = new model_jjb.spells.FireSpell(main.getRow(), main.getCol());
                                                String FACING = main.getDirection();

                                                javax.swing.Timer timer = new javax.swing.Timer(100, null);
                                                timer.addActionListener(new java.awt.event.ActionListener(){
                                                        @Override
                                                        public void actionPerformed(ActionEvent e) {
                                                                // TODO Auto-generated method stub
                                                                if(FACING.equals(MainCharacter.FACING_DOWN)){
                                                                        fireSpell.moveDown();
                                                                        fireDistance++;
                                                                }else if (FACING.equals(MainCharacter.FACING_LEFT)) {
                                                                        fireSpell.moveLeft();
                                                                        fireDistance++;
                                                                }else if (FACING.equals(MainCharacter.FACING_RIGHT)) {
                                                                        fireSpell.moveRight();
                                                                        fireDistance++;
                                                                }else if (FACING.equals(MainCharacter.FACING_UP)) {
                                                                        fireSpell.moveUp();
                                                                        fireDistance++;
                                                                }
                                                                if(fireDistance > 5){
                                                                        timer.stop();
                                                                        v.repaint();
                                                                        fireDistance = 0;
                                                                        fireSpell = null;
                                                                }
                                                                if(enemy.getCol() == fireSpell.getCol() && enemy.getRow() == fireSpell.getRow()){
                                                                        enemy.setAlive(false);
                                                                }else if (enemy2.getCol() == fireSpell.getCol() && enemy2.getRow() == fireSpell.getRow()) {
                                                                        enemy2.setAlive(false);
                                                                }else if (enemy3.getCol() == fireSpell.getCol() && enemy3.getRow() == fireSpell.getRow()) {
                                                                        enemy3.setAlive(false);
                                                                }else if (enemy4.getCol() == fireSpell.getCol() && enemy4.getRow() == fireSpell.getRow()) {
                                                                        enemy4.setAlive(false);
                                                                }else if (enemy5.getCol() == fireSpell.getCol() && enemy5.getRow() == fireSpell.getRow()) {
                                                                        enemy5.setAlive(false);
                                                                }
                                                                v.repaint();

                                                        }
                                                });
                                                timer.start();
                                        } else if(m.getCurrentState() == m.TEXT_STATE){
						m.setCurrentState(m.OVERWORLD_STATE);
					} else if(m.getCurrentState() == m.OVERWORLD_STATE){
						m.setCurrentState(m.TEXT_STATE);
						setCurrentMessage(m.getMessage(4));
					}
				}
				v.repaint();
			}
		});
	}
	
	public int getTime() {
		return time;
	}
	public Tile[][] getCurrentTile(){
		return m.getCurrentTile();
	}

	public int getCurrentState(){
		return m.getCurrentState();
	}

	public String getCurrentMessage(){
		return message;
	}

	public void setCurrentMessage(String message){
		this.message = message;
	}

	public model_jjb.characters.Character getCharacter(String name){
		return m.getCharacter("MAIN");
	}

	public model_jjb.spells.Spells getSpell() {
		return fireSpell;
	}

	public model_jjb.characters.Character getEnemy(String name){
		return enemy;
	}
	public model_jjb.characters.Character getEnemy2(String name){
		return enemy2;
	}
	public model_jjb.characters.Character getEnemy3(String name){
		return enemy3;
	}
	public model_jjb.characters.Character getEnemy4(String name){
		return enemy4;
	}
	public model_jjb.characters.Character getEnemy5(String name){
		return enemy5;
	}

	public boolean getEnemyAlive(){
		return enemy.isAlive();
	}
	public boolean getEnemy2Alive(){
		return enemy2.isAlive();
	}
	public boolean getEnemy3Alive(){
		return enemy3.isAlive();
	}
	public boolean getEnemy4Alive(){
		return enemy4.isAlive();
	}
	public boolean getEnemy5Alive(){
		return enemy5.isAlive();
	}
}
